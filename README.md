# Webots Übung 3 Lukas Scholz, Moritz Lindner

## Ausführen des Controllers

Da wir uns entschieden haben einen "KI"-Basierten Ansatz zu verwenden folgt eine kurze Anleitung die das Ausführen des
Controllers erklärt. Falls das Ausführen nicht möglich ist befinden sich im Ordner [Videos](videos) auch noch Videos die
das Verhalten zur Laufzeit zeigen sowie `.txt`-Detain der Logs des Supervisors.

1. Dependencies installieren: Wir empfehlen zum Ausführen ein Python Venv zu erstellen und dieses in Webots zu
   verwenden. In diesem Venv sollten die Dependencies der [requirements.txt](requirements.txt) installiert sein.

```shell
pip install -r requirements.txt
```

2. Des weitere besitzt dieses Repository ein Submodule welches [YoloV5](https://github.com/ultralytics/yolov5)
   beinhaltet. Dieses sollte mit ausgecheckt sein:

```shell
git submodule update --init
```
## Ergebnisse
| Welt  | Welt 0 | Welt 1 | Welt 2 | Welt 3 | Welt 4 |
|-------|--------|--------|--------|--------|--------|
| Score | 90%    | 90%    | 90%    | 90%    | 62%    |  
## Aufgabenstellung

### 3. Übung Visuelle Wahrnehmung (13.01. - 27.01.23)

In dieser Übung soll der Roboter ein Objekt mit seiner Kamera erkennen und es mit dem Kopf verfolgen. Das Ziel ist es
das Objekt möglichst genau im Zentrum des Bildes zu behalten.

#### Aufbau und Aufgabe

Als Grundlage nutzen wir die beiliegenden Webots-Szenarien `visual_tracking_nao`. In diesem Szenario soll der Roboter
Nao eine gelbe Ente im Bild erkennen und diese mit seinem Kopf verfolgen. Das Szenario besteht aus 5 Welten mit
Situationen von steigender Komplexität.

#### Ablauf und Score

Ein Experimental-Durchlauf beginnt 2s nach dem Start bzw. Zurücksetzen der Simulation und dauert 60s. In dieser Zeit
bewegt sich die Ente zufällig auf dem Feld.

In jedem Frame i wird ein Score-Wert s_i berechnet.

falls die Ente nicht im Bild ist, dann ist Score s_i = 0
falls die Ente im Bild ist, dann ist s_i proportional zur Entfernung der Ente zum Bild Mittelpunkt. Im Bildmittelpunkt
ist s_i = 1, ganz am Rand s_i = 0. Das Gesamtscore am Ende des Durchlaufes wird als der Durchschnittswert aller s_i in
Prozent berechnet. Der aktuelle Score-Wert wird ein mal pro Sekunde in der Webots-Konsole ausgegeben. Der finale Wert
wird nach dem Ende des Durchlaufes ausgegeben:

```
FINISHED:
--------------------
[60.00] Score: 44.29 / 1501 => 3%
```

Zur Orientierung: der beste erreichbare Score ist also 100%. Mit einem "Rumschauen" in dem Vorlage-Controller erreicht
der Roboter einen Wert von ca. 3%, stilles nach vorne Schauen erreicht ca. 15%. Der erreichte Wert geht anteilig in die
Berechnung der Punkte für die Aufgabe und sollte mindestens 50% betragen.

#### Aufgabe

Schreibt einen Controller, der die verschiedenen Szenarien lösen kann. Nutzt dazu die Vorlage visual_tracking.py und
ändert nicht den Namen des Controllers. Der Controller soll möglichst alle Welten lösen. Beschreibt in den Kommentaren
euer Vorgehen.

#### Teilaufgaben

1. visual_tracking_nao_0.wbt Das Feld ist leer. Die Ente bewegt sich in einem Bereich wo es gut von der Oberen Kamera
   erfasst werden kann. Es genügt das Objekt nur in der Oberen Kamera zu erkennen.
2. visual_tracking_nao_1.wbt Wie 0. Zusätzlich befinden sich weitere Objekte im Blickfeld.
3. visual_tracking_nao_2.wbt Wie 1. Zusätzlich stehen Menschen und andere Roboter im Blickfeld.
4. visual_tracking_nao_3.wbt Wie 2. Zusätzlich ist das Bild der Kamera verrauscht. Probiert verschiedene Rauschwerte
   aus.
5. visual_tracking_nao_4.wbt (Zusatz) Wie 3. Zusätzlich bewegt sich die Ente auch nach an den Roboter, wo es nur von der
   untereren Kamera gesehen werden kann. Um die Ente kontinuierlich zu verfolgen muss also die Erkennung in beiden
   Kameras erfolgen.

#### Hinweise

Die Aufgabe kann in der neuen Version von Webots R2023a bearbeitet werden. Dort muss der Simulator nicht mehr neu
gestartet werden, falls der Controller einen Syntax-Fehler hatte, es genügt das zurücksetzen mit dem Button [ << ]
https://cyberbotics.com/