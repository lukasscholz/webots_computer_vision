import math
from typing import Tuple

import cv2
import numpy as np
import torch

from controller import Robot, Motor


# calculate vertical field of view from a given horizontal field of view
def field_of_view_h_to_field_of_view_v(field_of_view_h: float, width: float, height: float) -> float:
    return math.degrees(2 * math.atan(math.tan(math.radians(field_of_view_h) * 0.5) * (height / width)))


CAMERA_WIDTH: int = 480
CAMERA_HEIGHT: int = 360
FIELD_OF_VIEW_H: float = 60.165
FIELD_OF_VIEW_V: float = field_of_view_h_to_field_of_view_v(FIELD_OF_VIEW_H, CAMERA_WIDTH, CAMERA_HEIGHT)
DEGREES_PER_PIXEL_H: float = FIELD_OF_VIEW_H / CAMERA_WIDTH
DEGREES_PER_PIXEL_V: float = FIELD_OF_VIEW_V / CAMERA_HEIGHT

# model path
model_path: str = "../../../run14_X_AdamW/weights/best.pt"
yolov5_path: str = "../../../yolov5"

# load model
print("loading model")
model = torch.hub.load(yolov5_path, "custom", path=model_path, source="local")
print("model loaded")

robot: Robot = Robot()

# we process each image every 40ms = 25fps
timestep: int = int(robot.getBasicTimeStep() * 4)

# enable devices
camera_top = robot.getDevice("CameraTop")
camera_top.enable(timestep)

width = camera_top.getWidth()
height = camera_top.getHeight()

camera_bottom = robot.getDevice("CameraBottom")
camera_bottom.enable(timestep)

head_yaw: Motor = robot.getDevice("HeadYaw")
head_yaw_position = head_yaw.getPositionSensor()
head_yaw_position.enable(timestep)
head_pitch: Motor = robot.getDevice("HeadPitch")
head_pitch_position = head_pitch.getPositionSensor()
head_pitch_position.enable(timestep)

# move arms down 
lShoulderPitch = robot.getDevice("LShoulderPitch")
rShoulderPitch = robot.getDevice("RShoulderPitch")
lShoulderPitch.setPosition(math.radians(90))
rShoulderPitch.setPosition(math.radians(90))


# create image from camera device
def getImage(camera):
    img = camera.getImage()
    img = np.frombuffer(img, dtype=np.uint8)
    img = img.reshape((height, width, 4))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img


# currently nothing is searched because we found that not moving is a valid strategy when losing the duck
def execute_search():
    # TODO: maybe implement that when loosing the ducke keep moving head in the same direction (vector based on last delta)
    print("waiting until object is detected again")


# checks if the models prediction is empty
def has_no_detection(predication) -> bool:
    # checks if the dataframe is empty
    return predication.xywh[0].size()[0] == 0


# calculate center from a given bounding box
def calculate_center(x1: float, y1: float, x2: float, y2: float) -> Tuple[float, float]:
    return (x1 + x2) / 2, (y1 + y2) / 2


# calculates the angle required to center the duck (resulting angle is unaware of global postion of the corresponding
# joint therefore "local")
def calculate_local_angle(center_position: float,
                          total_size: float,
                          degrees_per_pixel: float) -> float:
    distance_to_center = (total_size / 2) - center_position
    local_target_angle = degrees_per_pixel * distance_to_center
    return local_target_angle


while robot.step(timestep) != -1:
    # run prediction
    result = model(getImage(camera_top))

    if has_no_detection(result):
        execute_search()
    else:
        # read coordinates for bounding box (position) of the duck in the cameras image
        xyxy = result.xyxy[0][0]
        x1 = xyxy[0].item()
        y1 = xyxy[1].item()
        x2 = xyxy[2].item()
        y2 = xyxy[3].item()

        # get center coordinates of bound box
        (center_x, center_y) = calculate_center(x1, y1, x2, y2)

        # calculate yawn change (in degrees) required to center duck horizontally
        local_target_yawn = calculate_local_angle(center_x, CAMERA_WIDTH, DEGREES_PER_PIXEL_H)
        # calculate pitch change (in degrees) required to center duck vertically
        local_target_pitch = calculate_local_angle(center_y, CAMERA_HEIGHT, DEGREES_PER_PIXEL_V)

        # get current yawn and pitch
        current_yawn = math.degrees(head_yaw_position.getValue())
        current_pitch = math.degrees(head_pitch_position.getValue())

        # calculate new global yawn and pitch from current values and calculated change
        global_target_yawn = math.radians(current_yawn + local_target_yawn)
        global_target_pitch = math.radians(current_pitch - local_target_pitch)

        # set postion
        head_yaw.setPosition(global_target_yawn)
        head_pitch.setPosition(global_target_pitch)
