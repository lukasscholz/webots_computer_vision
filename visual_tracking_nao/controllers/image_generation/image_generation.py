import math
import os
import time

from controller import Robot

robot = Robot()

timestep = int(robot.getBasicTimeStep())

# change these variables to configure the image generation
image_target_amount: int = 100
time_delay_between_images_in_ms: int = 500
head_movement: bool = False

current_images: int = 0

# get access to the cameras and enable them
camera_top = robot.getDevice("CameraTop")
camera_top.enable(timestep)

## enable the bottom camera if necessary
camera_bottom = robot.getDevice("CameraBottom")
camera_bottom.enable(timestep)

width = camera_top.getWidth()
height = camera_top.getHeight()

head_yaw = robot.getDevice("HeadYaw")
head_pitch = robot.getDevice("HeadPitch")

# move arms down
lShoulderPitch = robot.getDevice("LShoulderPitch")
rShoulderPitch = robot.getDevice("RShoulderPitch")
lShoulderPitch.setPosition(math.radians(90))
rShoulderPitch.setPosition(math.radians(90))

base_path = "./images"
run_id_path = os.path.join(base_path, str(int(time.time())))
top_path = os.path.join(run_id_path, "top")
bottom_path = os.path.join(run_id_path, "bottom")


def createFolders():
    mode = 0o777
    if not os.path.exists(base_path):
        os.makedirs(base_path, mode)
    os.makedirs(run_id_path, mode)
    os.makedirs(top_path, mode)
    os.makedirs(bottom_path, mode)


createFolders()


# change behaviour in here to get other head movements
def execute_head_movement():
    t = robot.getTime()
    # calculate the target joints
    target_head_yaw = math.radians(100) * math.sin(t)
    target_head_pitch = math.radians(10) * math.cos(t)
    # set joints
    head_yaw.setPosition(target_head_yaw)
    head_pitch.setPosition(target_head_pitch)


def save_images():
    p = (current_images * 100) / image_target_amount
    print(
        f"saving image {current_images + 1} out of {image_target_amount} ({p}%) at {current_simulation_time() / 1000}s")
    camera_top.saveImage(f"{top_path}/{current_images}.png", 100)
    camera_bottom.saveImage(f"{bottom_path}/{current_images}.png", 100)


def time_since_last_image() -> int:
    return current_simulation_time() - simulation_timestamp_of_last_image


def current_simulation_time() -> int:
    return robot.getTime() * 1000


simulation_timestamp_of_last_image = 0

while robot.step(timestep) != -1 and current_images <= image_target_amount:
    if current_images < image_target_amount:
        if time_since_last_image() >= time_delay_between_images_in_ms:
            save_images()
            current_images = current_images + 1
            simulation_timestamp_of_last_image = current_simulation_time()

        if head_movement:
            execute_head_movement()
    else:
        current_images = current_images + 1
        print(f"created and saved all required {image_target_amount} images")
        print(f"top camera path: {os.path.abspath(top_path)}")
        print(f"bottom camera path: {os.path.abspath(bottom_path)}")